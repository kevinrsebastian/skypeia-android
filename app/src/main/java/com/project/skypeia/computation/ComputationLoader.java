package com.project.skypeia.computation;

import com.project.skypeia.Constants;
import com.project.skypeia.api.computation.LoadPaymentRetrofitService;
import com.project.skypeia.api.computation.SavePaymentRetrofitService;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ComputationLoader {

    private SavePaymentRetrofitService savePaymentRetrofitService;

    private LoadPaymentRetrofitService loadPaymentRetrofitService;

    public ComputationLoader() {
        // Instantiate Retrofit with JSON converter
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        savePaymentRetrofitService = retrofit.create(SavePaymentRetrofitService.class);
        loadPaymentRetrofitService = retrofit.create(LoadPaymentRetrofitService.class);
    }

    public void getComputation(Callback<Computation> callback,
                               String flightReservationId,
                               String hotelReservationId,
                               String couponId) {
        savePaymentRetrofitService.saveComputation(flightReservationId, hotelReservationId, couponId)
                              .enqueue(callback);
    }

    public void loadComputation(Callback<Computation> callback, String paymentId) {
        loadPaymentRetrofitService.getPaymentById(paymentId)
                                  .enqueue(callback);
    }
}
