package com.project.skypeia.computation;

public class Computation {

    public String id;

    public String paymentId;

    public float computedTotalFlights;

    public float computedTotalHotels;

    public float computedTotalCouponDiscounts;

    public float computedGrandTotal;
}
