package com.project.skypeia.computation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.api.reservation.SaveReservationRequestDto;
import com.project.skypeia.coupon.CouponValidator;
import com.project.skypeia.reservation.PaymentActivity;
import com.project.skypeia.reservation.Reservation;
import com.project.skypeia.reservation.ReservationSaver;
import com.project.skypeia.util.LoginUtils;
import com.project.skypeia.util.TextUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ComputationActivity extends AppCompatActivity{

    private String flightReservationId = null;

    private String hotelReservationId = null;

    private String couponId = null;

    private String computationId = null;

    private String reservationId = null;

    private LoginUtils loginUtils;

    @BindView(R.id.edit_text_coupon_code)
    EditText couponCodeEditText;

    @BindView(R.id.text_view_flights_price)
    TextView flightsPrice;

    @BindView(R.id.text_view_hotel_price)
    TextView hotelPrice;

    @BindView(R.id.text_view_coupon_discount)
    TextView couponDiscount;

    @BindView(R.id.text_view_total_price)
    TextView totalPrice;

    @BindView(R.id.confirm_button)
    Button confirmButton;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @OnClick(R.id.apply_code)
    public void onClickApply() {
        validateCoupon();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(couponCodeEditText.getWindowToken(), 0);
    }

    @OnClick(R.id.confirm_button)
    public void onClickConfirm() {
        saveReservation();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computation);
        ButterKnife.bind(this);
        setUpToolbar();

        getReservationIdsFtomIntent();
        loadComputation();

        loginUtils = new LoginUtils(getApplicationContext());
    }

    private void getReservationIdsFtomIntent() {
        flightReservationId = getIntent().getStringExtra(Constants.IntentExtraKeys.FLIGHT_RESERVATION_ID);
        if (getIntent().hasExtra(Constants.IntentExtraKeys.HOTEL_RESERVATION_ID)) {
            hotelReservationId = getIntent().getStringExtra(Constants.IntentExtraKeys.HOTEL_RESERVATION_ID);
        }
    }

    private void loadComputation() {
        Callback<Computation> callback = new Callback<Computation>() {
            @Override
            public void onResponse(Call<Computation> call,
                                   Response<Computation> response) {
                if (response.isSuccessful()) {
                    Computation computation = response.body();
                    computationId = computation.paymentId;
                    flightsPrice.setText(TextUtils.toCurrencyFormat(computation.computedTotalFlights));
                    hotelPrice.setText(TextUtils.toCurrencyFormat(computation.computedTotalHotels));
                    couponDiscount.setText(TextUtils.toCurrencyFormat(computation.computedTotalCouponDiscounts));
                    totalPrice.setText(TextUtils.toCurrencyFormat(computation.computedGrandTotal));
                    confirmButton.setEnabled(true);
                } else {
                    showFlightComputationSavingFailed();
                }
            }

            @Override
            public void onFailure(Call<Computation> call, Throwable t) {
                Timber.e(t.getMessage());
                showFlightComputationSavingFailed();
            }
        };

        new ComputationLoader().getComputation(callback, flightReservationId, hotelReservationId, couponId);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(getString(R.string.amounts_banner));
    }

    private void showFlightComputationSavingFailed() {
        Toast.makeText(ComputationActivity.this, "Saving computation failed", Toast.LENGTH_LONG)
             .show();
    }

    private void showInvalidCouponCode() {
        Toast.makeText(ComputationActivity.this, "Invalid Coupon Code", Toast.LENGTH_LONG)
             .show();
    }


    private void showReservationSavingFailed() {
        Toast.makeText(ComputationActivity.this, "Saving reservation failed", Toast.LENGTH_LONG)
             .show();
    }

    private void validateCoupon() {
        confirmButton.setEnabled(false);
        Callback<Boolean> callback = new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call,
                                   Response<Boolean> response) {
                if (response.isSuccessful()) {
                    if (response.body()) {
                        couponId = couponCodeEditText.getText().toString();
                        loadComputation();
                    } else {
                        couponId = null;
                        couponCodeEditText.setText("");
                        loadComputation();
                        showInvalidCouponCode();
                    }
                } else {
                    couponId = null;
                    couponCodeEditText.setText("");
                    loadComputation();
                    showInvalidCouponCode();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Timber.e(t.getMessage());
                couponId = null;
                couponCodeEditText.setText("");
                loadComputation();
                showInvalidCouponCode();
            }
        };

        new CouponValidator().validateCoupon(callback, couponCodeEditText.getText().toString());
    }

    private void saveReservation() {
        SaveReservationRequestDto saveReservationRequestDto = new SaveReservationRequestDto();
        saveReservationRequestDto.userId = loginUtils.getLoggedUserId();
        saveReservationRequestDto.flightReservationId = flightReservationId;
        saveReservationRequestDto.hotelReservationId = hotelReservationId;
        saveReservationRequestDto.couponId = couponId;
        saveReservationRequestDto.computationId = computationId;

        Callback<Reservation> callback = new Callback<Reservation>() {
            @Override
            public void onResponse(Call<Reservation> call,
                                   Response<Reservation> response) {
                if (response.isSuccessful()) {
                    Reservation reservation = response.body();
                    navigateToPaymentActivity(reservation.bookingId);
                } else {
                    showReservationSavingFailed();
                }
            }

            @Override
            public void onFailure(Call<Reservation> call, Throwable t) {
                Timber.e(t.getMessage());
                showReservationSavingFailed();
            }
        };

        new ReservationSaver().saveReservation(callback, saveReservationRequestDto);
    }

    private void navigateToPaymentActivity(String bookingId) {
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(Constants.IntentExtraKeys.BOOKING_ID, bookingId);
        intent.putExtra(Constants.IntentExtraKeys.TOTAL_AMOUNT, totalPrice.getText().toString());
        startActivity(intent);
        finish();
    }
}
