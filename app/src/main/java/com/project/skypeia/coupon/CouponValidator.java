package com.project.skypeia.coupon;

import com.project.skypeia.Constants;
import com.project.skypeia.api.coupon.ValidateCouponRetroService;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CouponValidator {

    private ValidateCouponRetroService validateCouponRetroService;

    public CouponValidator() {
        // Instantiate Retrofit with JSON converter
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        validateCouponRetroService = retrofit.create(ValidateCouponRetroService.class);
    }

    public void validateCoupon(Callback<Boolean> callback,
                               String couponId) {
        validateCouponRetroService.validateCoupon(couponId)
                                  .enqueue(callback);
    }
}
