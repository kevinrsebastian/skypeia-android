package com.project.skypeia.passenger;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.project.skypeia.Constants;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Does not have a screen but calls the PassengerFormActivity
 * depending on the number of dependents.
 */
public class PassengersActivity extends AppCompatActivity {

    private int passengerCurrentCount = 0;

    private int passengerTotalCount;

    private Passenger[] passengers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getPassengerCountFromIntent();
        navigateToPassengersActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Passenger passenger = data.getParcelableExtra(Constants.IntentExtraKeys.PASSENGER_DETAILS);
            passengers[passengerCurrentCount] = passenger;
            passengerCurrentCount++;

            // Check if the all passenger details have been created
            if (passengerCurrentCount < passengerTotalCount) {
                navigateToPassengersActivity();
            } else {
                // Return to HomeActivity, passing the list of passengers
                Intent intent = new Intent();

                // Convert array to ArrayList
                ArrayList passengerList = new ArrayList();
                passengerList.addAll(Arrays.asList(passengers));

                intent.putExtra(Constants.IntentExtraKeys.PASSENGER_DETAILS, passengerList);
                setResult(RESULT_OK, intent);
                finish();
            }
        } else {
            if (passengerCurrentCount > 0) {
                passengerCurrentCount--;
                navigateToPassengersActivity();
            } else {
                onBackPressed();
            }
        }
    }

    private void getPassengerCountFromIntent() {
        passengerTotalCount =
            getIntent().getIntExtra(Constants.IntentExtraKeys.PASSENGER_TOTAL_COUNT, 0);
        passengers = new Passenger[passengerTotalCount];
    }

    /**
     * Call the passenger form activity passing the current and total number of passengers
     */
    private void navigateToPassengersActivity() {
        Intent intent = new Intent(this, PassengerFormActivity.class);
        intent.putExtra(Constants.IntentExtraKeys.PASSENGER_CURRENT_COUNT, passengerCurrentCount);
        intent.putExtra(Constants.IntentExtraKeys.PASSENGER_TOTAL_COUNT, passengerTotalCount);

        if (passengers[passengerCurrentCount] != null) {
            intent.putExtra(Constants.IntentExtraKeys.PASSENGER_DETAILS, passengers[passengerCurrentCount]);
        }

        startActivityForResult(intent, 1);
    }
}
