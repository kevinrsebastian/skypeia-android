package com.project.skypeia.passenger;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.project.skypeia.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PassengerViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.text_view_name)
    TextView nameTextView;

    @BindView(R.id.text_view_email)
    TextView emailTextView;

    @BindView(R.id.text_view_phone)
    TextView phoneTextView;

    @BindView(R.id.text_view_birth_date)
    TextView birthDateTextView;

    @BindView(R.id.text_view_has_baggage_allowance)
    TextView hasBaggageAllowanceTextView;

    @BindView(R.id.text_view_has_meal_allowance)
    TextView hasMealAllowanceTextView;

    public PassengerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
