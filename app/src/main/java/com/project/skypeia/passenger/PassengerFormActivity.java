package com.project.skypeia.passenger;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.util.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Input the passenger details, depending on the passengerType
 */
public class PassengerFormActivity extends AppCompatActivity {

    private Calendar calendar;

    private int currentCount;

    private int totalCount;

    private String[] types = {"Senior Citizen", "Adult", "Child", "Infant"};

    @BindString(R.string.required)
    String required;

    @BindView(R.id.text_input_layout_name)
    TextInputLayout nameTextInputLayout;

    @BindView(R.id.edit_text_name)
    EditText nameEditText;

    @BindView(R.id.text_input_layout_email)
    TextInputLayout emailTextInputLayout;

    @BindView(R.id.edit_text_email)
    EditText emailEditText;

    @BindView(R.id.text_input_layout_phone)
    TextInputLayout phoneTextInputLayout;

    @BindView(R.id.edit_text_phone)
    EditText phoneEditText;

    @BindView(R.id.text_input_layout_birth_date)
    TextInputLayout birthDateTextInputLayout;

    @BindView(R.id.edit_text_birth_date)
    EditText birthDateEditText;

    @BindView(R.id.has_baggage_allowance)
    Switch hasBaggageAllowanceSwitch;

    @BindView(R.id.has_meal_allowance)
    Switch hasMealAllowanceSwitch;

    @BindView(R.id.text_input_layout_passenger_type)
    TextInputLayout passengerTypeTextInputLayout;

    @BindView(R.id.edit_text_passenger_type)
    EditText passengerTypeDateEditText;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.button_back)
    Button backButton;

    @OnTextChanged(R.id.edit_text_name)
    public void onNameChanged(CharSequence input) {
        validateNameIsProvided();
    }

    @OnTextChanged(R.id.edit_text_email)
    public void onEmailChanged(CharSequence input) {
        validateEmailIsProvided();
    }

    @OnTextChanged(R.id.edit_text_phone)
    public void onPhoneChanged(CharSequence input) {
        validatePhoneIsProvided();
    }

    @OnClick(R.id.edit_text_birth_date)
    public void onClickBirthDateEditText() {
        // Listener for when a date is selected
        DatePickerDialog.OnDateSetListener onDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                // Update return calendar and display
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateDates();
                }
        };

        // Show dialog for selecting a date
        DatePickerDialog datePickerDialog =
            new DatePickerDialog(PassengerFormActivity.this,
                                 onDateSetListener,
                                 calendar.get(Calendar.YEAR),
                                 calendar.get(Calendar.MONTH),
                                 calendar.get(Calendar.DAY_OF_MONTH));

        // Enable only dates after departureDate
        Calendar currentCalendar = Calendar.getInstance();
        datePickerDialog.getDatePicker().setMaxDate(currentCalendar.getTimeInMillis());
        datePickerDialog.show();
    }

    @OnClick(R.id.edit_text_passenger_type)
    public void onClickPassengerTypeEditText() {
        new AlertDialog.Builder(PassengerFormActivity.this)
            .setTitle(getString(R.string.select_passenger_type))
            .setItems(types, new DialogInterface.OnClickListener() {
                // Update passenger type display
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    passengerTypeDateEditText.setText(types[i]);
                }
            })
            .create()
            .show();
    }

    @OnTextChanged(R.id.edit_text_birth_date)
    public void onBirthDateChanged(CharSequence input) {
        validateBirthDateIsProvided();
    }

    @OnTextChanged(R.id.edit_text_passenger_type)
    public void onPassengerTypeChanged(CharSequence input) {
        validatePassengerTypeIsProvided();
    }

    @OnClick(R.id.button_back)
    public void onClickBackButton() {
        onBackPressed();
    }

    @OnClick(R.id.button_next)
    public void onClickNextButton() {
        if (validateFields()) {
            Intent intent = new Intent();
            intent.putExtra(Constants.IntentExtraKeys.PASSENGER_DETAILS, buildPassenger());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passenger_form);
        ButterKnife.bind(this);

        calendar = Calendar.getInstance();
        getTotalAndCurrentCountFromIntent();
        getPassengerFromIntent();
        setUpToolbar();
        setUpButtons();
    }

    private Passenger buildPassenger() {
        Passenger passenger = new Passenger();
        passenger.name = nameEditText.getText().toString();
        passenger.email = emailEditText.getText().toString();
        passenger.phone = phoneEditText.getText().toString();
        passenger.birthDate = TimeUtils.toIso8601(calendar.getTimeInMillis());
        passenger.hasBaggageAllowance = hasBaggageAllowanceSwitch.isChecked();
        passenger.hasMealAllowance = hasMealAllowanceSwitch.isChecked();

        String passengerType = passengerTypeDateEditText.getText().toString();
        switch (passengerType) {
            case "Senior Citizen":
                passenger.memberType = "SENIOR_CITIZEN";
                break;
            case "Adult":
                passenger.memberType = "ADULT";
                break;
            case "Child":
                passenger.memberType = "CHILD";
                break;
            case "Infant":
                passenger.memberType = "INFANT";
                break;
        }

        return passenger;
    }

    private void getPassengerFromIntent() {
        // Set UI values if a passenger is passed
        if (getIntent().hasExtra(Constants.IntentExtraKeys.PASSENGER_DETAILS)) {
            Passenger passenger = getIntent().getParcelableExtra(Constants.IntentExtraKeys.PASSENGER_DETAILS);
            nameEditText.setText(passenger.name);
            emailEditText.setText(passenger.email);
            phoneEditText.setText(passenger.phone);

            calendar.setTimeInMillis(TimeUtils.fromIso8601(passenger.birthDate));
            updateDates();

            hasBaggageAllowanceSwitch.setChecked(passenger.hasBaggageAllowance);
            hasMealAllowanceSwitch.setChecked(passenger.hasMealAllowance);

            switch (passenger.memberType) {
                case "SENIOR_CITIZEN":
                    passengerTypeDateEditText.setText("Senior Citizen");
                    break;
                case "ADULT":
                    passengerTypeDateEditText.setText("Adult");
                    break;
                case "CHILD":
                    passengerTypeDateEditText.setText("Child");
                    break;
                case "INFANT":
                    passengerTypeDateEditText.setText("Infant");
                    break;
            }
        }
    }

    private void getTotalAndCurrentCountFromIntent() {
        currentCount = getIntent().getIntExtra(Constants.IntentExtraKeys.PASSENGER_CURRENT_COUNT, 0);
        totalCount = getIntent().getIntExtra(Constants.IntentExtraKeys.PASSENGER_TOTAL_COUNT, 0);
    }

    private void setUpButtons() {
        if (currentCount == 0) {
            backButton.setVisibility(View.INVISIBLE);
        }
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbarTitle.setText("Passenger Details " + (currentCount + 1) + " of " + totalCount);
    }

    private void updateDates() {
        String myFormat = "MM-dd-yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        birthDateEditText.setText(sdf.format(calendar.getTime()));
    }

    private boolean validateNameIsProvided() {
        if (nameEditText.getText().toString().length() < 1) {
            nameTextInputLayout.setErrorEnabled(true);
            nameTextInputLayout.setError("*" + required);
            return false;
        } else {
            nameTextInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateEmailIsProvided() {
        if (emailEditText.getText().toString().length() < 1) {
            emailTextInputLayout.setErrorEnabled(true);
            emailTextInputLayout.setError("*" + required);
            return false;
        } else {
            emailTextInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validatePhoneIsProvided() {
        if (phoneEditText.getText().toString().length() < 1) {
            phoneTextInputLayout.setErrorEnabled(true);
            phoneTextInputLayout.setError("*" + required);
            return false;
        } else {
            phoneTextInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateBirthDateIsProvided() {
        if (birthDateEditText.getText().toString().length() < 1) {
            birthDateTextInputLayout.setErrorEnabled(true);
            birthDateTextInputLayout.setError("*" + required);
            return false;
        } else {
            birthDateTextInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validatePassengerTypeIsProvided() {
        if (passengerTypeDateEditText.getText().toString().length() < 1) {
            passengerTypeTextInputLayout.setErrorEnabled(true);
            passengerTypeTextInputLayout.setError("*" + required);
            return false;
        } else {
            passengerTypeTextInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateFields() {
        return validateNameIsProvided()
                   & validateEmailIsProvided()
                   & validatePhoneIsProvided()
                   & validateBirthDateIsProvided()
                   & validatePassengerTypeIsProvided();
    }
}
