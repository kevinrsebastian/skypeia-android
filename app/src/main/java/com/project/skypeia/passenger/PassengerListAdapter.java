package com.project.skypeia.passenger;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.skypeia.R;
import com.project.skypeia.util.TimeUtils;

import java.util.List;

public class PassengerListAdapter extends RecyclerView.Adapter<PassengerViewHolder> {

    private List<Passenger> passengers;

    public PassengerListAdapter(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    @Override
    public PassengerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_passenger, parent, false);
        return new PassengerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PassengerViewHolder holder, final int position) {
        Passenger passenger = passengers.get(position);
        holder.nameTextView.setText(passenger.name);
        holder.emailTextView.setText(passenger.email);
        holder.phoneTextView.setText(passenger.phone);
        holder.birthDateTextView.setText(TimeUtils.toBirthDateDisplayFormat(TimeUtils.fromIso8601(passenger.birthDate)));
        if (passenger.hasBaggageAllowance) {
            holder.hasBaggageAllowanceTextView.setVisibility(View.VISIBLE);
        }
        if (passenger.hasMealAllowance) {
            holder.hasMealAllowanceTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return passengers.size();
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
        notifyDataSetChanged();
    }
}
