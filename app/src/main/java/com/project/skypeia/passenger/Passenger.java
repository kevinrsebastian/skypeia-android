package com.project.skypeia.passenger;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Passenger implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Passenger createFromParcel(Parcel in) {
            return new Passenger(in);
        }

        public Passenger[] newArray(int size) {
            return new Passenger[size];
        }
    };

    @SerializedName("passengerName")
    public String name;

    public String email;

    public String phone;

    public String birthDate;

    public boolean hasBaggageAllowance;

    public boolean hasMealAllowance;

    public String memberType;

    // Default constructor
    public Passenger() {
        this.name = "";
        this.email = "";
        this.phone = "";
        this.birthDate = "";
        this.hasBaggageAllowance = false;
        this.hasMealAllowance = hasMealAllowance;
        this.memberType = "";
    }

    // Parcelable constructor
    public Passenger(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.birthDate = in.readString();
        this.hasBaggageAllowance = in.readByte() != 0;
        this.hasMealAllowance = in.readByte() != 0;
        this.memberType = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Must be in order to be retrieved correctly
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.birthDate);
        dest.writeByte((byte) (this.hasBaggageAllowance ? 1: 0));
        dest.writeByte((byte) (this.hasMealAllowance ? 1: 0));
        dest.writeString(this.memberType);
    }
}
