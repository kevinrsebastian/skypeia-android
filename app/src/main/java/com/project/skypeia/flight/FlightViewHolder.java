package com.project.skypeia.flight;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.project.skypeia.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FlightViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_flight)
    View flightItem;

    @BindView(R.id.radio_button_selection)
    RadioButton selection;

    @BindView(R.id.text_view_flight_number)
    TextView flightNumber;

    @BindView(R.id.text_view_departure_time)
    TextView departureTime;

    @BindView(R.id.text_view_arrival_time)
    TextView arrivalTime;

    @BindView(R.id.text_view_origin)
    TextView orginAirport;

    @BindView(R.id.text_view_destination)
    TextView destinationAirport;

    @BindView(R.id.text_view_duration)
    TextView duration;

    @BindView(R.id.text_view_total_fare)
    TextView totalFare;

    public FlightViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
