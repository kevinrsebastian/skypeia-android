package com.project.skypeia.flight;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.project.skypeia.airport.Airport;

public class Flight implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Flight createFromParcel(Parcel in) {
            return new Flight(in);
        }

        public Flight[] newArray(int size) {
            return new Flight[size];
        }
    };

    @SerializedName("flightId")
    public String id;

    @SerializedName("flightNumber")
    public String number;

    @SerializedName("originPort")
    public Airport originAirport;

    @SerializedName("destinationPort")
    public Airport destinationAirport;

    public String departureTime;

    public String arrivalTime;

    public long departureDateTime;

    public long arrivalDateTime;

    @SerializedName("estimatedFlightDuration")
    public String duration;

    public String airline;

    public String aircraftType;

    public int availableSeats;

    public float baseFare;

    public float baggageAllowance;

    public float mealAllowance;

    public float taxesAndFees;

    // Default constructor
    public Flight() {
        this.id = "";
        this.number = "";
        this.departureTime = "";
        this.arrivalTime = "";
        this.departureDateTime = 0;
        this.arrivalDateTime = 0;
        this.duration = "";
        this.airline = "";
        this.aircraftType = "";
        this.availableSeats = 0;
        this.baseFare = 0;
        this.baggageAllowance = 0;
        this.mealAllowance = 0;
        this.taxesAndFees = 0;
        this.originAirport = null;
        this.destinationAirport = null;
    }

    // Default constructor
    public Flight(Parcel in) {
        this.id = in.readString();
        this.number = in.readString();
        this.departureTime = in.readString();
        this.arrivalTime = in.readString();
        this.departureDateTime = in.readLong();
        this.arrivalDateTime = in.readLong();
        this.duration = in.readString();
        this.airline = in.readString();
        this.aircraftType = in.readString();
        this.availableSeats = in.readInt();
        this.baseFare = in.readFloat();
        this.baggageAllowance = in.readFloat();
        this.mealAllowance = in.readFloat();
        this.taxesAndFees = in.readFloat();
        this.originAirport = null;
        this.originAirport = in.readParcelable(Airport.class.getClassLoader());
        this.destinationAirport = in.readParcelable(Airport.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Must be in order to be retrieved correctly
        dest.writeString(this.id);
        dest.writeString(this.number);
        dest.writeString(this.departureTime);
        dest.writeString(this.arrivalTime);
        dest.writeLong(this.departureDateTime);
        dest.writeLong(this.arrivalDateTime);
        dest.writeString(this.duration);
        dest.writeString(this.airline);
        dest.writeString(this.aircraftType);
        dest.writeInt(this.availableSeats);
        dest.writeFloat(this.baseFare);
        dest.writeFloat(this.baggageAllowance);
        dest.writeFloat(this.mealAllowance);
        dest.writeFloat(this.taxesAndFees);
        dest.writeParcelable(originAirport, flags);
        dest.writeParcelable(destinationAirport, flags);
    }
}
