package com.project.skypeia.flight;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.api.flight.FilterFlightsRequestDto;
import com.project.skypeia.api.flight.FilterFlightsResponseDto;
import com.project.skypeia.home.FlightSearchCriteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


import static com.project.skypeia.util.TimeUtils.addDays;
import static com.project.skypeia.util.TimeUtils.toIso8601;

/**
 * Shows the flights selection.
 */
public class FlightSelectionActivity extends AppCompatActivity {

    private List<Flight> flights = new ArrayList<>();

    private FlightListAdapter adapter;

    private String flightType;

    private FlightSearchCriteria flightSearchCriteria;

    @BindView(R.id.next_button)
    Button nextButton;

    @BindView(R.id.no_flights)
    TextView noFlights;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @OnClick(R.id.next_button)
    public void onClickNextButton() {
        Flight flight = adapter.getSelectedFlight();

        switch (flightType) {
            case Constants.DEPARTURE :
                flight.originAirport = flightSearchCriteria.originAirport;
                flight.destinationAirport = flightSearchCriteria.destinationAirport;
                break;
            case Constants.RETURN :
                flight.originAirport = flightSearchCriteria.destinationAirport;
                flight.destinationAirport = flightSearchCriteria.originAirport;
                break;
        }

        Intent intent = new Intent();
        intent.putExtra(Constants.IntentExtraKeys.RESULT_FLIGHT, flight);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_selection);
        ButterKnife.bind(this);

        getFlightCriteriaFromIntent();
        setUpToolbar();
        setUpRecyclerView();
        setUpFlights();
    }

    private void getFlightCriteriaFromIntent() {
        flightSearchCriteria =
            getIntent().getParcelableExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_CRITERIA);
        flightType = getIntent().getStringExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_TYPE);
    }

    private void setUpFlights() {
        // Filter criteria
        FilterFlightsRequestDto filterFlightsRequestDto = new FilterFlightsRequestDto();
        switch (flightType) {
            case Constants.DEPARTURE :
                filterFlightsRequestDto.origin = flightSearchCriteria.originAirport.id;
                filterFlightsRequestDto.destination = flightSearchCriteria.destinationAirport.id;
                filterFlightsRequestDto.startDepartureTime =
                    toIso8601(flightSearchCriteria.departureMillis);
                filterFlightsRequestDto.endDepartureTime =
                    toIso8601(addDays(flightSearchCriteria.departureMillis, 1));
                break;
            case Constants.RETURN :
                filterFlightsRequestDto.origin = flightSearchCriteria.destinationAirport.id;
                filterFlightsRequestDto.destination = flightSearchCriteria.originAirport.id;
                filterFlightsRequestDto.startDepartureTime =
                    toIso8601(flightSearchCriteria.returnMillis);
                filterFlightsRequestDto.endDepartureTime =
                    toIso8601(addDays(flightSearchCriteria.returnMillis, 1));
                break;
        }
        filterFlightsRequestDto.availableSeats = flightSearchCriteria.passengerCount;

        // Callback for loading flights
        Callback<FilterFlightsResponseDto> callback = new Callback<FilterFlightsResponseDto>() {
            @Override
            public void onResponse(Call<FilterFlightsResponseDto> call,
                                   Response<FilterFlightsResponseDto> response) {
                if (response.isSuccessful()) {
                    Flight[] flightArray = response.body().results;
                    if (flightArray != null) {
                        flights = Arrays.asList(flightArray);
                        adapter.setFlights(flights);
                        nextButton.setEnabled(true);
                        recyclerView.setVisibility(View.VISIBLE);
                    } else {
                        nextButton.setEnabled(false);
                        noFlights.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<FilterFlightsResponseDto> call, Throwable t) {
                Timber.e(t.getMessage());
                Toast.makeText(FlightSelectionActivity.this,
                               "Loading flights failed",
                               Toast.LENGTH_LONG)
                     .show();
            }
        };

        new FlightLoader().getFlights(callback, filterFlightsRequestDto);
    }

    private void setUpRecyclerView() {
        adapter = new FlightListAdapter(this, flights);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        switch (flightType) {
            case Constants.DEPARTURE :
                toolbarTitle.setText(getString(R.string.select_departure_flight));
                break;
            case Constants.RETURN :
                toolbarTitle.setText(getString(R.string.select_return_flight));
                break;
        }
    }
}
