package com.project.skypeia.flight;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.skypeia.R;
import com.project.skypeia.util.TextUtils;
import com.project.skypeia.util.TimeUtils;

import java.util.List;

public class FlightListAdapter extends RecyclerView.Adapter<FlightViewHolder> {

    private Context context;

    private List<Flight> flights;

    private int lastCheckedPosition = -1;

    public FlightListAdapter(Context context, List<Flight> flights) {
        this.context = context;
        this.flights = flights;
    }

    @Override
    public FlightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_flight, parent, false);
        return new FlightViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FlightViewHolder holder, final int position) {
        // Get the corresponding flight and bind to the ViewHolder
        Flight flight = flights.get(position);
        holder.flightNumber.setText(flight.number);
        long departureTime = TimeUtils.fromIso8601(flight.departureTime);
        holder.departureTime.setText(TimeUtils.toHourMinute(departureTime));
        long arrivalTime = TimeUtils.fromIso8601(flight.arrivalTime);
        holder.arrivalTime.setText(TimeUtils.toHourMinute(arrivalTime));
        holder.orginAirport.setText(flight.originAirport.id);
        holder.destinationAirport.setText(flight.destinationAirport.id);
        holder.duration.setText(flight.duration);
        holder.totalFare.setText("PHP " + TextUtils.toCurrencyFormat(flight.baseFare + flight.taxesAndFees));

        // Set the ui attributes of the selected item
        if (position == lastCheckedPosition) {
            holder.selection.setChecked(true);
        } else {
            holder.selection.setChecked(false);
        }

        // Set a listener that sets the currently clicked item as the last checked position
        // then restart the items
        holder.selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastCheckedPosition = position;
                notifyDataSetChanged();
            }
        });

        // Set the default selected
        if (position == 0 && lastCheckedPosition == -1) {
            holder.selection.setChecked(true);
            lastCheckedPosition = 0;
        }
    }

    @Override
    public int getItemCount() {
        return flights.size();
    }

    public Flight getSelectedFlight() {
        return flights.get(lastCheckedPosition);
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
        notifyDataSetChanged();
    }
}
