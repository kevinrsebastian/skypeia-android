package com.project.skypeia.flight;

import com.project.skypeia.Constants;
import com.project.skypeia.api.flight.FilterFlightsRequestDto;
import com.project.skypeia.api.flight.FilterFlightsResponseDto;
import com.project.skypeia.api.flight.FlightsRetrofitService;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FlightLoader {

    private FlightsRetrofitService flightsRetrofitService;

    public FlightLoader() {
        // Instantiate Retrofit with JSON converter
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        flightsRetrofitService = retrofit.create(FlightsRetrofitService.class);
    }

    public void getFlights(Callback<FilterFlightsResponseDto> callback,
                           FilterFlightsRequestDto filterFlightsRequestDto) {
        flightsRetrofitService.getFlights(filterFlightsRequestDto)
                              .enqueue(callback);
    }

    public void getFlight(Callback<Flight> callback, String flightId) {
        flightsRetrofitService.getFlightById(flightId)
                              .enqueue(callback);
    }
}
