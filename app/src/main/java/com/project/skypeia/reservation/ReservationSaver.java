package com.project.skypeia.reservation;

import com.project.skypeia.Constants;
import com.project.skypeia.api.reservation.ReservationRetrofitService;
import com.project.skypeia.api.reservation.SaveReservationRequestDto;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReservationSaver {

    private ReservationRetrofitService reservationRetrofitService;

    public ReservationSaver() {
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        reservationRetrofitService = retrofit.create(ReservationRetrofitService.class);
    }

    public void saveReservation(Callback<Reservation> callback,
                                SaveReservationRequestDto saveReservationRequestDto) {
        reservationRetrofitService.saveReservation(saveReservationRequestDto)
                                  .enqueue(callback);
    }
}
