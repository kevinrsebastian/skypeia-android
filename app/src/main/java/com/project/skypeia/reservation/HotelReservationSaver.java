package com.project.skypeia.reservation;

import com.project.skypeia.Constants;
import com.project.skypeia.api.reservation.ReservationRetrofitService;
import com.project.skypeia.api.reservation.SaveHotelReservationRequestDto;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HotelReservationSaver {

    private ReservationRetrofitService reservationRetrofitService;

    public HotelReservationSaver() {
        // Instantiate Retrofit with JSON converter
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        reservationRetrofitService = retrofit.create(ReservationRetrofitService.class);
    }

    public void saveHotelReservation(
        Callback<HotelReservation> callback,
        SaveHotelReservationRequestDto saveHotelReservationRequestDto) {
        reservationRetrofitService.saveHotelReservation(saveHotelReservationRequestDto)
                                  .enqueue(callback);
    }
}
