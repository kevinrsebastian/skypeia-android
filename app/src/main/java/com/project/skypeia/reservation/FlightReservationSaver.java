package com.project.skypeia.reservation;

import com.project.skypeia.Constants;
import com.project.skypeia.api.reservation.SaveFlightReservationRequestDto;
import com.project.skypeia.api.reservation.ReservationRetrofitService;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FlightReservationSaver {

    private ReservationRetrofitService reservationRetrofitService;

    public FlightReservationSaver() {
        // Instantiate Retrofit with JSON converter
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        reservationRetrofitService = retrofit.create(ReservationRetrofitService.class);
    }

    public void saveFlightReservation(
            Callback<FlightReservation> callback,
            SaveFlightReservationRequestDto saveFlightReservationRequestDto) {
        reservationRetrofitService.saveFlightReservation(saveFlightReservationRequestDto)
                                  .enqueue(callback);
    }
}
