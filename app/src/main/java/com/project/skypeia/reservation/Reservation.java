package com.project.skypeia.reservation;

import android.os.Parcel;
import android.os.Parcelable;

public class Reservation implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Reservation createFromParcel(Parcel in) {
            return new Reservation(in);
        }

        public Reservation[] newArray(int size) {
            return new Reservation[size];
        }
    };

    public String id;

    public String bookingId;

    public String flightReservationId;

    public String hotelReservationId;

    public String couponId;

    public String computationId;

    public String userId;

    public String createDate;

    public String status;

    public Reservation() {
        this.id = id;
        this.bookingId = bookingId;
        this.flightReservationId = flightReservationId;
        this.hotelReservationId = hotelReservationId;
        this.couponId = couponId;
        this.computationId = computationId;
        this.userId = userId;
        this.createDate = createDate;
        this.status = status;
    }

    public Reservation(Parcel in) {
        this.id = in.readString();
        this.bookingId = in.readString();
        this.flightReservationId = in.readString();
        this.hotelReservationId = in.readString();
        this.couponId = in.readString();
        this.computationId = in.readString();
        this.userId = in.readString();
        this.createDate = in.readString();
        this.status = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Must be in order to be retrieved correctly
        dest.writeString(this.id);
        dest.writeString(this.bookingId);
        dest.writeString(this.flightReservationId);
        dest.writeString(this.hotelReservationId);
        dest.writeString(this.couponId);
        dest.writeString(this.computationId);
        dest.writeString(this.userId);
        dest.writeString(this.createDate);
        dest.writeString(this.status);
    }
}
