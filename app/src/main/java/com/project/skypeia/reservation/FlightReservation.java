package com.project.skypeia.reservation;

import com.project.skypeia.passenger.Passenger;

public class FlightReservation {

    public String id;

    public String flightReservationId;

    public String oneWayFlightId;

    public String roundTripFlightId;

    public Passenger[] flightPassengerDetails;

    public String userId;
}
