package com.project.skypeia.reservation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.api.reservation.SaveFlightReservationRequestDto;
import com.project.skypeia.api.reservation.SaveHotelReservationRequestDto;
import com.project.skypeia.computation.ComputationActivity;
import com.project.skypeia.flight.Flight;
import com.project.skypeia.home.FlightSearchCriteria;
import com.project.skypeia.hotel.Hotel;
import com.project.skypeia.hotel.HotelSelectionActivity;
import com.project.skypeia.passenger.Passenger;
import com.project.skypeia.passenger.PassengerListAdapter;
import com.project.skypeia.user.ProfileActivity;
import com.project.skypeia.user.UserLoginActivity;
import com.project.skypeia.util.LoginUtils;
import com.project.skypeia.widget.FlightCard;
import com.project.skypeia.widget.HotelCard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Shows the summary of flight reservation details including the departure and return flight ids,
 * and the list of passengers.
 */
public class ReservationSummaryActivity extends AppCompatActivity {

    // Request IDs
    private static final int LOGIN_USER_REQUEST = 0;
    private static final int SELECT_HOTEL_REQUEST = 1;
    private static final int COMPUTATION_REQUEST = 2;

    private FlightSearchCriteria flightSearchCriteria;

    private List<Passenger> passengers = new ArrayList<>();

    private PassengerListAdapter adapter;

    private Flight departureFlight;

    private Flight returnFlight;

    private int hasBaggageAllowanceCount = 0;

    private int hasMealAllowanceCount = 0;

    private Hotel selectedHotel;

    private String flightReservationId;

    private String hotelReservationId;

    private LoginUtils loginUtils;

    private MenuItem loginMenuItem;

    private MenuItem myReservationsMenuItem;

    @BindView(R.id.scroll)
    ScrollView scrollView;

    @BindView(R.id.flight_card_departure)
    FlightCard departureFlightCard;

    @BindView(R.id.flight_card_return)
    FlightCard returnFlightCard;

    @BindView(R.id.hotel_card)
    HotelCard hotelCard;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @OnClick(R.id.select_hotel_button)
    public void onClickSelectHotelButton() {
        navigateToHotelSelectionActivity();
    }

    @OnClick(R.id.next_button)
    public void onClickNextButton() {
        if (loginUtils.getLoggedUserId() == null) {
            navigateToUserLoginActivity();
        } else {
            saveFlightReservation();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_summary);
        ButterKnife.bind(this);
        setUpToolbar();
        setUpRecyclerView();
        getPassengersAngFlightsFromIntent();
        countPassengerAllowanceFees();
        showDepartureFlightCard();

        // Show returnFlight card if provided
        if (returnFlight != null) {
            showReturnFlightCard();
        }

        loginUtils = new LoginUtils(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (selectedHotel != null) {
            showHotelCard();
        } else {
            hotelCard.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        toggleMenuItems();
        switch (requestCode) {
            case SELECT_HOTEL_REQUEST:
                if (resultCode == RESULT_OK) {
                    selectedHotel = data.getParcelableExtra(Constants.IntentExtraKeys.HOTEL);
                } else {
                    selectedHotel = null;
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        loginMenuItem = menu.findItem(R.id.login);
        myReservationsMenuItem = menu.findItem(R.id.my_reservations);
        toggleMenuItems();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.login :
                navigateToUserLoginActivity();
                return true;
            case R.id.my_reservations :
                navigateToUserProfileActivity();
                return true;
        }
        return true;
    }

    private void countPassengerAllowanceFees() {
        for (Passenger passenger : passengers) {
            if (passenger.hasBaggageAllowance) {
                hasBaggageAllowanceCount++;
            }
            if (passenger.hasMealAllowance) {
                hasMealAllowanceCount++;
            }
        }
    }

    private void getPassengersAngFlightsFromIntent() {
        flightSearchCriteria = getIntent().getParcelableExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_CRITERIA);
        passengers = getIntent().getParcelableArrayListExtra(Constants.IntentExtraKeys.PASSENGERS);
        departureFlight = getIntent().getParcelableExtra(Constants.IntentExtraKeys.DEPARTURE_FLIGHT);
        if (getIntent().hasExtra(Constants.IntentExtraKeys.RETURN_FLIGHT)) {
            returnFlight = getIntent().getParcelableExtra(Constants.IntentExtraKeys.RETURN_FLIGHT);
        }
        adapter.setPassengers(passengers);
    }

    private void navigateToComputationActivity() {
        Intent intent = new Intent(this, ComputationActivity.class);
        intent.putExtra(Constants.IntentExtraKeys.FLIGHT_RESERVATION_ID, flightReservationId);
        if (selectedHotel != null) {
            intent.putExtra(Constants.IntentExtraKeys.HOTEL_RESERVATION_ID, hotelReservationId);
        }
        startActivityForResult(intent, COMPUTATION_REQUEST);
    }

    /**
     * Navigate to the hotel selection activity passing the flight selection criteria
     */
    private void navigateToHotelSelectionActivity() {
        Intent intent = new Intent(this, HotelSelectionActivity.class);
        intent.putExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_CRITERIA, flightSearchCriteria);
        startActivityForResult(intent, SELECT_HOTEL_REQUEST);
    }

    private void navigateToUserLoginActivity() {
        Intent intent = new Intent(this, UserLoginActivity.class);
        startActivityForResult(intent, LOGIN_USER_REQUEST);
    }

    private void navigateToUserProfileActivity() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivityForResult(intent, 9);
    }

    private void saveFlightReservation() {
        // Flight Reservation
        SaveFlightReservationRequestDto saveFlightReservationRequestDto = new SaveFlightReservationRequestDto();
        saveFlightReservationRequestDto.userId = loginUtils.getLoggedUserId();
        saveFlightReservationRequestDto.oneWayFlightId = departureFlight.id;
        if (returnFlight != null) {
            saveFlightReservationRequestDto.roundTripFlightId = returnFlight.id;
            saveFlightReservationRequestDto.flightType = "ROUND_TRIP";
        }
        saveFlightReservationRequestDto.flightPassengerDetails = passengers.toArray(new Passenger[passengers.size()]);

        // Callback for saving flight reservations
        Callback<FlightReservation> callback = new Callback<FlightReservation>() {
            @Override
            public void onResponse(Call<FlightReservation> call,
                                   Response<FlightReservation> response) {
                if (response.isSuccessful()) {
                    flightReservationId = response.body().flightReservationId;
                    if (selectedHotel != null) {
                        saveHotelReservation();
                    } else {
                        navigateToComputationActivity();
                    }
                } else {
                    showFlightReservationSavingFailed();
                }
            }

            @Override
            public void onFailure(Call<FlightReservation> call, Throwable t) {
                Timber.e(t.getMessage());
                showFlightReservationSavingFailed();
            }
        };

        new FlightReservationSaver().saveFlightReservation(callback, saveFlightReservationRequestDto);
    }

    private void saveHotelReservation() {
        // Hotel Reservation
        SaveHotelReservationRequestDto saveHotelReservationRequestDto = new SaveHotelReservationRequestDto();
        saveHotelReservationRequestDto.hotelId = selectedHotel.hotelId;
        saveHotelReservationRequestDto.userId = loginUtils.getLoggedUserId();
        saveHotelReservationRequestDto.totalNumOfPax = flightSearchCriteria.passengerCount;

        // Callback for saving hotel reservations
        Callback<HotelReservation> callback = new Callback<HotelReservation>() {
            @Override
            public void onResponse(Call<HotelReservation> call,
                                   Response<HotelReservation> response) {
                if (response.isSuccessful()) {
                    hotelReservationId = response.body().hotelReservationId;
                    navigateToComputationActivity();
                } else {
                    showHotelReservationSavingFailed();
                }
            }

            @Override
            public void onFailure(Call<HotelReservation> call, Throwable t) {
                Timber.e(t.getMessage());
                showHotelReservationSavingFailed();
            }
        };

        new HotelReservationSaver().saveHotelReservation(callback, saveHotelReservationRequestDto);
    }

    private void showHotelCard() {
        // Calculate departurePrice
        float total = selectedHotel.hotelCostPrePax * flightSearchCriteria.passengerCount;

        hotelCard.setHotelDetails(selectedHotel, total);
        hotelCard.setVisibility(View.VISIBLE);
    }

    private void showDepartureFlightCard() {
        // Calculate departurePrice
        float totalFareWithTax = (departureFlight.baseFare + departureFlight.taxesAndFees) * passengers.size();
        float totalBaggageAllowance = departureFlight.baggageAllowance * hasBaggageAllowanceCount;
        float totalMealAllowance = departureFlight.mealAllowance * hasMealAllowanceCount;
        float total = totalFareWithTax + totalBaggageAllowance + totalMealAllowance;

        departureFlightCard.setFlightDetails(departureFlight, total);
    }

    private void showFlightReservationSavingFailed() {
        Toast.makeText(ReservationSummaryActivity.this, "Saving flight reservation failed", Toast.LENGTH_LONG)
             .show();
    }

    private void showHotelReservationSavingFailed() {
        Toast.makeText(ReservationSummaryActivity.this, "Saving hotel reservation failed", Toast.LENGTH_LONG)
             .show();
    }

    private void showReturnFlightCard() {
        // Calculate returnPrice
        float totalFareWithTax = (returnFlight.baseFare + returnFlight.taxesAndFees) * passengers.size();
        float totalBaggageAllowance = returnFlight.baggageAllowance * hasBaggageAllowanceCount;
        float totalMealAllowance = returnFlight.mealAllowance * hasMealAllowanceCount;
        float total = totalFareWithTax + totalBaggageAllowance + totalMealAllowance;

        returnFlightCard.setReturnLabel();
        returnFlightCard.setFlightDetails(returnFlight, total);
        returnFlightCard.setVisibility(View.VISIBLE);
    }

    private void setUpRecyclerView() {
        adapter = new PassengerListAdapter(passengers);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(getString(R.string.reservation_summary));
    }

    private void toggleMenuItems() {
        if (loginUtils.getLoggedUserId() != null) {
            loginMenuItem.setVisible(false);
            myReservationsMenuItem.setVisible(true);
        } else {
            loginMenuItem.setVisible(true);
            myReservationsMenuItem.setVisible(false);
        }
    }
}
