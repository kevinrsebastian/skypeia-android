package com.project.skypeia.reservation;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.project.skypeia.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReservationViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_hotel)
    View hotelItem;

    @BindView(R.id.text_view_booking_id)
    TextView bookingId;

    @BindView(R.id.text_view_booking_date)
    TextView bookingDate;

    @BindView(R.id.text_view_status)
    TextView status;

    public ReservationViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
