package com.project.skypeia.reservation;

import com.project.skypeia.Constants;
import com.project.skypeia.api.FilterRequestDto;
import com.project.skypeia.api.reservation.LoadReservationsResultDto;
import com.project.skypeia.api.reservation.ReservationRetrofitService;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReservationLoader {

    private ReservationRetrofitService reservationRetrofitService;

    public ReservationLoader() {
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        reservationRetrofitService = retrofit.create(ReservationRetrofitService.class);
    }

    public void loadReservations(Callback<LoadReservationsResultDto> callback) {
        reservationRetrofitService.getReservations(new FilterRequestDto(0, 0))
                                  .enqueue(callback);
    }
}
