package com.project.skypeia.reservation;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.user.UserReservationActivity;
import com.project.skypeia.util.TimeUtils;

import java.util.List;

public class ReservationListAdapter extends RecyclerView.Adapter<ReservationViewHolder> {

    private Context context;

    private List<Reservation> reservations;

    private View.OnClickListener onClickListener;

    public ReservationListAdapter(Context context, List<Reservation> reservations) {
        this.context = context;
        this.reservations = reservations;
    }

    @Override
    public ReservationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_reservation, parent, false);
        return new ReservationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReservationViewHolder holder, final int position) {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Navigate to UserReservationActivity
                Intent intent = new Intent(context, UserReservationActivity.class);
                intent.putExtra(Constants.IntentExtraKeys.RESERVATION, reservations.get(position));
                context.startActivity(intent);
            }
        };

        Reservation reservation = reservations.get(position);
        holder.bookingId.setText(reservation.bookingId);
        holder.bookingDate.setText(TimeUtils.toFlightDisplayFormat(TimeUtils.fromIso8601(reservation.createDate)));
        holder.status.setText(reservation.status);
        holder.hotelItem.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return reservations.size();
    }

    public void setFlights(List<Reservation> reservations) {
        this.reservations = reservations;
        notifyDataSetChanged();
    }
}
