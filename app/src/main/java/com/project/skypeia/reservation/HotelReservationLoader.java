package com.project.skypeia.reservation;

import com.project.skypeia.Constants;
import com.project.skypeia.api.reservation.ReservationRetrofitService;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HotelReservationLoader {

    private ReservationRetrofitService reservationRetrofitService;

    public HotelReservationLoader() {
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        reservationRetrofitService = retrofit.create(ReservationRetrofitService.class);
    }

    public void loadHotelReservation(Callback<HotelReservation> callback, String hotelReservationId) {
        reservationRetrofitService.getHotelReservationById(hotelReservationId)
                                  .enqueue(callback);
    }
}
