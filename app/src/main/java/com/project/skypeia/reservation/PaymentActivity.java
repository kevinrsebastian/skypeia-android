package com.project.skypeia.reservation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.home.HomeActivity;
import com.project.skypeia.user.ProfileActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentActivity extends AppCompatActivity {

    @BindView(R.id.text_view_booking_no)
    TextView bookingNo;

    @BindView(R.id.text_view_amount)
    TextView amount;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @OnClick(R.id.done_button)
    public void onClickDoneButton() {
        clearActivityStackAndNavigateToProfileActivity();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        setUpToolbar();

        bookingNo.setText(getIntent().getStringExtra((Constants.IntentExtraKeys.BOOKING_ID)));
        amount.setText(getIntent().getStringExtra((Constants.IntentExtraKeys.TOTAL_AMOUNT)));
    }

    @Override
    public void onBackPressed() {
        clearActivityStackAndNavigateToHomeActivity();
    }

    private void clearActivityStackAndNavigateToProfileActivity() {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void clearActivityStackAndNavigateToHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Payment Details");
    }
}
