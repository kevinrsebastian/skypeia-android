package com.project.skypeia;

public class Constants {

    public static final String BASE_URL = "http://104.199.214.252:8080/ProjectSkypeia/api/";

    public static final String DEPARTURE = "DEPARTURE";
    public static final String RETURN = "RETURN";

    public static class IntentExtraKeys {
        public static final String FLIGHT_SELECTION_CRITERIA = "FLIGHT_SELECTION_CRITERIA";
        public static final String FLIGHT_SELECTION_TYPE = "FLIGHT_SELECTION_TYPE";
        public static final String PASSENGER_CURRENT_COUNT = "PASSENGER_CURRENT_COUNT";
        public static final String PASSENGER_TOTAL_COUNT = "PASSENGER_TOTAL_COUNT";
        public static final String PASSENGER_DETAILS = "PASSENGER_DETAILS";
        public static final String RESULT_FLIGHT = "RESULT_FLIGHT";

        public static final String DEPARTURE_FLIGHT = "DEPARTURE_FLIGHT";
        public static final String RETURN_FLIGHT = "RETURN_FLIGHT";
        public static final String PASSENGERS = "PASSENGERS";

        public static final String FLIGHT_RESERVATION_ID = "FLIGHT_RESERVATION_ID";
        public static final String HOTEL_RESERVATION_ID = "HOTEL_RESERVATION_ID";

        public static final String BOOKING_ID = "BOOKING_ID";
        public static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";

        public static final String HOTEL = "HOTEL";
        public static final String RESERVATION = "RESERVATION";
    }
}
