package com.project.skypeia.home;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Toast;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.airport.Airport;
import com.project.skypeia.airport.AirportLoader;
import com.project.skypeia.api.airport.FilterAirportsResponseDto;
import com.project.skypeia.flight.Flight;
import com.project.skypeia.flight.FlightSelectionActivity;
import com.project.skypeia.passenger.Passenger;
import com.project.skypeia.passenger.PassengersActivity;
import com.project.skypeia.reservation.ReservationSummaryActivity;
import com.project.skypeia.user.ProfileActivity;
import com.project.skypeia.user.UserLoginActivity;
import com.project.skypeia.util.LoginUtils;
import com.project.skypeia.util.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * The first screen shown to the user. Shows fields for filtering flights
 * and orchestrates the screens to be shown.
 */
public class HomeActivity extends AppCompatActivity {

    // Request IDs
    private static final int LOGIN_USER_REQUEST = 0;
    private static final int SELECT_DEPARTURE_FLIGHT_REQUEST = 1;
    private static final int SELECT_RETURN_FLIGHT_REQUEST = 2;
    private static final int PASSENGERS_REQUEST = 3;
    private static final int RESERVATION_REQUEST = 4;

    private Calendar departureCalendar;

    private Calendar returnCalendar;

    private String[] airportLabels;

    private List<Airport> airportList;

    private Boolean roundTrip = true;

    private FlightSearchCriteria flightSearchCriteria;

    private Flight departureFlight;

    private Flight returnFlight;

    private ArrayList<Passenger> passengers;

    private String flightReservationId;

    private LoginUtils loginUtils;

    private MenuItem loginMenuItem;

    private MenuItem myReservationsMenuItem;

    @BindString(R.string.required)
    String required;

    @BindView(R.id.edit_text_departure)
    EditText departureEditText;

    @BindView(R.id.text_input_layout_departure)
    TextInputLayout departureTextInputLayout;

    @BindView(R.id.edit_text_destination)
    EditText destinationEditText;

    @BindView(R.id.text_input_layout_destination)
    TextInputLayout destinationTextInputLayout;

    @BindView(R.id.number_picker_passenger_count)
    NumberPicker numberPickerPassengerCount;

    @BindView(R.id.edit_text_origin)
    EditText originEditText;

    @BindView(R.id.text_input_layout_origin)
    TextInputLayout originTextInputLayout;

    @BindView(R.id.edit_text_return)
    EditText returnEditText;

    @BindView(R.id.text_input_layout_return)
    TextInputLayout returnTextInputLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.book_flight)
    public void onClickBookFlightButton() {
        if (validateFields()) {
            navigateToFlightSelectionActivity(SELECT_DEPARTURE_FLIGHT_REQUEST);
        }
    }

    @OnClick(R.id.edit_text_destination)
    public void onClickDestinationEditText() {
        // Show dialog for selecting a destination
        new AlertDialog.Builder(HomeActivity.this)
            .setTitle(getString(R.string.select_destination))
            .setItems(airportLabels, new DialogInterface.OnClickListener() {
                // Update destination display
                @Override
                public void onClick(DialogInterface dialog, int position) {
                    destinationEditText.setText(airportList.get(position).id);
                }
            })
            .create()
            .show();
    }

    @OnTextChanged(R.id.edit_text_destination)
    public void onDestinationChanged(CharSequence input) {
        validateDestinationIsProvided();
    }

    @OnClick(R.id.edit_text_origin)
    public void onClickOriginEditText() {
        // Show dialog for selecting a destination
        new AlertDialog.Builder(HomeActivity.this)
            .setTitle(getString(R.string.select_origin))
            .setItems(airportLabels, new DialogInterface.OnClickListener() {
                // Update origin display
                @Override
                public void onClick(DialogInterface dialog, int position) {
                    originEditText.setText(airportList.get(position).id);
                }
            })
            .create()
            .show();
    }

    @OnTextChanged(R.id.edit_text_origin)
    public void onOriginChanged(CharSequence input) {
        validateOriginIsProvided();
    }

    @OnClick(R.id.edit_text_departure)
    public void onClickDepartureEditText() {
        // Listener for when a date is selected
        DatePickerDialog.OnDateSetListener onDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                // Update departure calendar and display
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    departureCalendar.set(Calendar.YEAR, year);
                    departureCalendar.set(Calendar.MONTH, month);
                    departureCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateDates();
                }
            };

        // Show dialog for selecting a date
        new DatePickerDialog(HomeActivity.this,
                             onDateSetListener,
                             departureCalendar.get(Calendar.YEAR),
                             departureCalendar.get(Calendar.MONTH),
                             departureCalendar.get(Calendar.DAY_OF_MONTH))
            .show();
    }

    @OnClick(R.id.edit_text_return)
    public void onClickReturnEditText() {
        // Listener for when a date is selected
        DatePickerDialog.OnDateSetListener onDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                // Update return calendar and display
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    returnCalendar.set(Calendar.YEAR, year);
                    returnCalendar.set(Calendar.MONTH, month);
                    returnCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateDates();
                }
            };

        // Show dialog for selecting a date
        DatePickerDialog datePickerDialog =
            new DatePickerDialog(HomeActivity.this,
                                 onDateSetListener,
                                 returnCalendar.get(Calendar.YEAR),
                                 returnCalendar.get(Calendar.MONTH),
                                 returnCalendar.get(Calendar.DAY_OF_MONTH));

        // Enable only dates after departureDate
        datePickerDialog.getDatePicker().setMinDate(departureCalendar.getTimeInMillis());
        datePickerDialog.show();
    }

    @OnTextChanged(R.id.edit_text_return)
    public void onReturnChange(CharSequence input) {
        validateReturnIsNotBeforeDeparture();
    }

    @OnClick({R.id.radio_button_round_trip, R.id.radio_button_one_way})
    public void onClickTripType(RadioButton radioButton) {
        // Get the id of the checked RadioButton
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()) {
            case R.id.radio_button_round_trip:
                if (checked) {
                    roundTrip = true;
                    returnTextInputLayout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.radio_button_one_way:
                if (checked) {
                    roundTrip = false;
                    returnTextInputLayout.setVisibility(View.INVISIBLE);
                }
                break;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        loadAirports();
        setUpToolbar();
        setUpCalendars();
        setUpCounters();

        loginUtils = new LoginUtils(getApplicationContext());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        toggleMenuItems();
        switch (requestCode) {
            case SELECT_DEPARTURE_FLIGHT_REQUEST :
                if (resultCode == RESULT_OK) {
                    departureFlight = data.getParcelableExtra(Constants.IntentExtraKeys.RESULT_FLIGHT);
                    if (roundTrip) {
                        navigateToFlightSelectionActivity(SELECT_RETURN_FLIGHT_REQUEST);
                    } else {
                        returnFlight = null;
                        navigateToPassengersActivity();
                    }
                }
                break;
            case SELECT_RETURN_FLIGHT_REQUEST :
                if (resultCode == RESULT_OK) {
                    returnFlight = data.getParcelableExtra(Constants.IntentExtraKeys.RESULT_FLIGHT);
                    navigateToPassengersActivity();
                }
                break;
            case PASSENGERS_REQUEST :
                if (resultCode == RESULT_OK) {
                    passengers = data.getParcelableArrayListExtra(Constants.IntentExtraKeys.PASSENGER_DETAILS);
                    navigateToReservationSummaryActivity(passengers);
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        loginMenuItem = menu.findItem(R.id.login);
        myReservationsMenuItem = menu.findItem(R.id.my_reservations);
        toggleMenuItems();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.login :
                navigateToUserLoginActivity();
                return true;
            case R.id.my_reservations :
                navigateToUserProfileActivity();
                return true;
        }
        return true;
    }

    /**
     * Build the FlightSearchCriteria POJO that gets passed between activities
     */
    private FlightSearchCriteria buildFlightSearchCriteria() {
        flightSearchCriteria = new FlightSearchCriteria();
        flightSearchCriteria.roundTrip = this.roundTrip;

        String originId = originEditText.getText().toString();
        flightSearchCriteria.originAirport = getAirportById(originId);

        String destinationId = destinationEditText.getText().toString();
        flightSearchCriteria.destinationAirport = getAirportById(destinationId);

        flightSearchCriteria.departureMillis = departureCalendar.getTimeInMillis();
        flightSearchCriteria.returnMillis = returnCalendar.getTimeInMillis();
        flightSearchCriteria.passengerCount = numberPickerPassengerCount.getValue();
        return flightSearchCriteria;
    }

    /**
     * Get the Airport POJO by its id
     */
    private Airport getAirportById(String id) {
        for (Airport airport : airportList) {
            if (airport.id.equals(id))
                return airport;
        }
        return null;
    }

    /**
     * Load the list of airports from the API
     */
    private void loadAirports() {
        // Callback for loading airports
        Callback<FilterAirportsResponseDto> callback = new Callback<FilterAirportsResponseDto>() {
            @Override
            public void onResponse(Call<FilterAirportsResponseDto> call,
                                   Response<FilterAirportsResponseDto> response) {
                if (response.isSuccessful()) {
                    // Set up airport list and labels
                    Airport[] ports = response.body().results;
                    airportList = Arrays.asList(ports);

                    airportLabels = new String[airportList.size()];
                    for (int i = 0; i < airportList.size(); i++) {
                        airportLabels[i] = airportList.get(i).toLabel();
                    }
                } else {
                    showAirportsLoadingFailed();
                }
            }

            @Override
            public void onFailure(Call<FilterAirportsResponseDto> call, Throwable t) {
                Timber.e(t.getMessage());
                showAirportsLoadingFailed();
            }
        };

        new AirportLoader().getAirports(callback);
    }

    /**
     * Call the flight selection activity passing the flight selection criteria and
     * the type of flight to select (departure or return)
     */
    private void navigateToFlightSelectionActivity(int flightRequestType) {
        Intent intent = new Intent(this, FlightSelectionActivity.class);
        intent.putExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_CRITERIA,
                        buildFlightSearchCriteria());
        switch (flightRequestType) {
            case SELECT_DEPARTURE_FLIGHT_REQUEST :
                intent.putExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_TYPE,
                                Constants.DEPARTURE);
                break;
            case SELECT_RETURN_FLIGHT_REQUEST:
                intent.putExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_TYPE,
                                Constants.RETURN);
                break;
        }

        startActivityForResult(intent, flightRequestType);
    }

    /**
     * Call the flight reservation summary activity
     */
    private void navigateToReservationSummaryActivity(ArrayList<Passenger> passengers) {
        Intent intent = new Intent(this, ReservationSummaryActivity.class);
        intent.putExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_CRITERIA, buildFlightSearchCriteria());
        intent.putParcelableArrayListExtra(Constants.IntentExtraKeys.PASSENGERS, passengers);
        intent.putExtra(Constants.IntentExtraKeys.DEPARTURE_FLIGHT, departureFlight);
        if (roundTrip) {
            intent.putExtra(Constants.IntentExtraKeys.RETURN_FLIGHT, returnFlight);
        }

        startActivityForResult(intent, RESERVATION_REQUEST);
    }

    /**
     * Call the passengers activity passing the flight selection criteria
     */
    private void navigateToPassengersActivity() {
        Intent intent = new Intent(this, PassengersActivity.class);
        intent.putExtra(Constants.IntentExtraKeys.PASSENGER_TOTAL_COUNT,
                        flightSearchCriteria.passengerCount);

        startActivityForResult(intent, PASSENGERS_REQUEST);
    }

    private void navigateToUserLoginActivity() {
        Intent intent = new Intent(this, UserLoginActivity.class);
        startActivityForResult(intent, LOGIN_USER_REQUEST);
    }

    private void navigateToUserProfileActivity() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivityForResult(intent, 9);
    }

    private void setUpCalendars() {
        long currentDate = TimeUtils.removeTimeUnits(Calendar.getInstance().getTimeInMillis());
        departureCalendar = Calendar.getInstance();
        departureCalendar.setTimeInMillis(currentDate);

        returnCalendar = Calendar.getInstance();
        returnCalendar.setTimeInMillis(currentDate);
        updateDates();
    }

    private void setUpCounters() {
        numberPickerPassengerCount.setValue(1);
        numberPickerPassengerCount.setMinValue(1);
        numberPickerPassengerCount.setMaxValue(40);
        numberPickerPassengerCount.setWrapSelectorWheel(false);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void showAirportsLoadingFailed() {
        Toast.makeText(HomeActivity.this, "Loading airports failed", Toast.LENGTH_LONG)
             .show();
    }

    private void toggleMenuItems() {
        if (loginUtils.getLoggedUserId() != null) {
            loginMenuItem.setVisible(false);
            myReservationsMenuItem.setVisible(true);
        } else {
            loginMenuItem.setVisible(true);
            myReservationsMenuItem.setVisible(false);
        }
    }

    /**
     * Update the display dates for departure and return
     */
    private void updateDates() {
        String myFormat = "MM-dd-yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        departureEditText.setText(sdf.format(departureCalendar.getTime()));
        returnEditText.setText(sdf.format(returnCalendar.getTime()));
    }

    private boolean validateFields() {
        return validateOriginIsProvided()
            & validateDestinationIsProvided()
            & validateReturnIsNotBeforeDeparture();
    }

    private boolean validateDestinationIsProvided() {
        if (destinationEditText.getText().toString().length() < 1) {
            destinationTextInputLayout.setErrorEnabled(true);
            destinationTextInputLayout.setError("*" + required);
            return false;
        } else {
            destinationTextInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateOriginIsProvided() {
        if (originEditText.getText().toString().length() < 1) {
            originTextInputLayout.setErrorEnabled(true);
            originTextInputLayout.setError("*" + required);
            return false;
        } else {
            originTextInputLayout.setErrorEnabled(false);
            return true;
        }
    }

    private boolean validateReturnIsNotBeforeDeparture() {
        if (!roundTrip || returnCalendar.getTimeInMillis() >= departureCalendar.getTimeInMillis()) {
            returnTextInputLayout.setErrorEnabled(false);
            return true;
        } else {
            returnTextInputLayout.setErrorEnabled(true);
            returnTextInputLayout.setError("*Cannot be before departure");
            return false;
        }
    }
}
