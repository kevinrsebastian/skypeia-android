package com.project.skypeia.home;

import android.os.Parcel;
import android.os.Parcelable;

import com.project.skypeia.airport.Airport;

public class FlightSearchCriteria implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public FlightSearchCriteria createFromParcel(Parcel in) {
            return new FlightSearchCriteria(in);
        }

        public Airport[] newArray(int size) {
            return new Airport[size];
        }
    };

    public boolean roundTrip;

    public Airport originAirport;

    public Airport destinationAirport;

    public Long departureMillis;

    public Long returnMillis;

    public int passengerCount;

    // Default constructor
    public FlightSearchCriteria() {
        this.departureMillis = 0L;
        this.returnMillis = 0L;
        this.passengerCount = 1;
    }

    // Parcelable Constructor
    public FlightSearchCriteria(Parcel dest) {
        this.roundTrip = dest.readByte() != 0;
        this.originAirport = dest.readParcelable(Airport.class.getClassLoader());
        this.destinationAirport = dest.readParcelable(Airport.class.getClassLoader());
        this.departureMillis = dest.readLong();
        this.returnMillis = dest.readLong();
        this.passengerCount = dest.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Must be in order to be retrieved correctly
        dest.writeByte((byte) (this.roundTrip ? 1: 0));
        dest.writeParcelable(this.originAirport, flags);
        dest.writeParcelable(this.destinationAirport, flags);
        dest.writeLong(this.departureMillis);
        dest.writeLong(this.returnMillis);
        dest.writeInt(this.passengerCount);
    }
}
