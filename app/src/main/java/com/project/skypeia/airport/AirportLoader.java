package com.project.skypeia.airport;

import com.project.skypeia.Constants;
import com.project.skypeia.api.FilterRequestDto;
import com.project.skypeia.api.airport.AirportRetrofitService;
import com.project.skypeia.api.airport.FilterAirportsResponseDto;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AirportLoader {

    private AirportRetrofitService airportRetrofitService;

    public AirportLoader() {
        // Instantiate Retrofit with JSON converter
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        airportRetrofitService = retrofit.create(AirportRetrofitService.class);
    }

    public void getAirports(Callback<FilterAirportsResponseDto> callback) {
        airportRetrofitService.getPorts(new FilterRequestDto(0, 0))
                               .enqueue(callback);
    }
}
