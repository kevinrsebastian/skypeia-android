package com.project.skypeia.airport;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Airport implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Airport createFromParcel(Parcel in) {
            return new Airport(in);
        }

        public Airport[] newArray(int size) {
            return new Airport[size];
        }
    };

    @SerializedName("portId")
    public String id;

    @SerializedName("description")
    public String description;

    @SerializedName("airport")
    public String name;

    // Default constructor
    public Airport() {
        this.id = "";
        this.description = "";
        this.name = "";
    }

    // Parcelable constructor
    public Airport(Parcel in) {
        this.id = in.readString();
        this.description = in.readString();
        this.name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Must be in order to be retrieved correctly
        dest.writeString(this.id);
        dest.writeString(this.description);
        dest.writeString(this.name);
    }

    public String toLabel() {
        return description + " (" + id + ")";
    }
}
