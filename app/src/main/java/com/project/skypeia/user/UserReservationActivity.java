package com.project.skypeia.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.computation.Computation;
import com.project.skypeia.computation.ComputationLoader;
import com.project.skypeia.flight.Flight;
import com.project.skypeia.flight.FlightLoader;
import com.project.skypeia.hotel.Hotel;
import com.project.skypeia.hotel.HotelLoader;
import com.project.skypeia.passenger.Passenger;
import com.project.skypeia.passenger.PassengerListAdapter;
import com.project.skypeia.reservation.FlightReservation;
import com.project.skypeia.reservation.FlightReservationLoader;
import com.project.skypeia.reservation.HotelReservation;
import com.project.skypeia.reservation.HotelReservationLoader;
import com.project.skypeia.reservation.Reservation;
import com.project.skypeia.util.TextUtils;
import com.project.skypeia.util.TimeUtils;
import com.project.skypeia.widget.FlightCard;
import com.project.skypeia.widget.HotelCard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class UserReservationActivity extends AppCompatActivity {

    private List<Flight> flights = new ArrayList<>();

    private List<Passenger> passengers = new ArrayList<>();

    private Flight departureFlight;

    private Flight returnFlight;

    private Computation computation;

    private Hotel hotel;

    private HotelReservation hotelReservation;

    private FlightReservation flightReservation;

    private Reservation reservation;

    private PassengerListAdapter adapter;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.scroll)
    ScrollView scrollView;

    @BindView(R.id.card_view_reservation)
    CardView reservationCardView;

    @BindView(R.id.text_view_booking_id)
    TextView bookingIdTextView;

    @BindView(R.id.text_view_booking_date)
    TextView bookingDateTextView;

    @BindView(R.id.text_view_status)
    TextView statusTextView;

    @BindView(R.id.text_view_total_amount)
    TextView totalAmountTextView;

    @BindView(R.id.flight_card_departure)
    FlightCard departureFlightCard;

    @BindView(R.id.flight_card_return)
    FlightCard returnFlightCard;

    @BindView(R.id.hotel_card)
    HotelCard hotelCard;

    @BindView(R.id.card_view_passenger)
    CardView passengersCardView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_reservation);
        ButterKnife.bind(this);
        setUpToolbar();
        setUpRecyclerView();
        getReservationFromIntent();
        loadFlightReservation();
    }

    private void getReservationFromIntent() {
        reservation = getIntent().getParcelableExtra(Constants.IntentExtraKeys.RESERVATION);
    }

    private void loadFlightReservation() {
        Callback<FlightReservation> callback = new Callback<FlightReservation>() {
            @Override
            public void onResponse(Call<FlightReservation> call,
                                   Response<FlightReservation> response) {
                if (response.isSuccessful()) {
                    flightReservation = response.body();
                    passengers = Arrays.asList(flightReservation.flightPassengerDetails);
                    loadFlight(false);
                } else {
                    showFlightReservationLoadingFailed();
                }
            }

            @Override
            public void onFailure(Call<FlightReservation> call, Throwable t) {
                Timber.e(t.getMessage());
                showFlightReservationLoadingFailed();
            }
        };

        new FlightReservationLoader().loadFlightReservation(callback, reservation.flightReservationId);
    }

    private void loadFlight(final boolean isReturn) {
        Callback<Flight> callback = new Callback<Flight>() {
            @Override
            public void onResponse(Call<Flight> call,
                                   Response<Flight> response) {
                if (response.isSuccessful()) {
                    if (!isReturn) {
                        departureFlight = response.body();
                        if (flightReservation.roundTripFlightId != null
                                && !flightReservation.roundTripFlightId.equals("")) {
                            loadFlight(true);
                        } else {
                            loadHotelReservation();
                        }
                    } else {
                        returnFlight = response.body();
                        loadHotelReservation();
                    }
                } else {
                    showFlightLoadingFailed();
                }
            }

            @Override
            public void onFailure(Call<Flight> call, Throwable t) {
                Timber.e(t.getMessage());
                showFlightLoadingFailed();
            }
        };

        if (!isReturn) {
            new FlightLoader().getFlight(callback, flightReservation.oneWayFlightId);
        } else {
            new FlightLoader().getFlight(callback, flightReservation.roundTripFlightId);
        }
    }

    private void loadHotelReservation() {
        // Skip if there are no hotel reservations
        if (reservation.hotelReservationId == null || reservation.hotelReservationId.equals("")) {
            loadPayment();
            return;
        }

        Callback<HotelReservation> callback = new Callback<HotelReservation>() {
            @Override
            public void onResponse(Call<HotelReservation> call,
                                   Response<HotelReservation> response) {
                if (response.isSuccessful()) {
                    hotelReservation = response.body();
                    loadHotel(hotelReservation.hotelId);
                } else {
                    showHotelReservationLoadingFailed();
                }
            }

            @Override
            public void onFailure(Call<HotelReservation> call, Throwable t) {
                Timber.e(t.getMessage());
                showHotelReservationLoadingFailed();
            }
        };

        new HotelReservationLoader().loadHotelReservation(callback, reservation.hotelReservationId);
    }

    private void loadHotel(String hotelId) {
        Callback<Hotel> callback = new Callback<Hotel>() {
            @Override
            public void onResponse(Call<Hotel> call,
                                   Response<Hotel> response) {
                if (response.isSuccessful()) {
                    hotel = response.body();
                    loadPayment();
                } else {
                    showHotelReservationLoadingFailed();
                }
            }

            @Override
            public void onFailure(Call<Hotel> call, Throwable t) {
                Timber.e(t.getMessage());
                showHotelReservationLoadingFailed();
            }
        };

        new HotelLoader().getHotel(callback, hotelId);
    }

    private void loadPayment() {
        Callback<Computation> callback = new Callback<Computation>() {
            @Override
            public void onResponse(Call<Computation> call,
                                   Response<Computation> response) {
                if (response.isSuccessful()) {
                    computation = response.body();
                    showCards();
                } else {
                    showPaymentLoadingFailed();
                }
            }

            @Override
            public void onFailure(Call<Computation> call, Throwable t) {
                Timber.e(t.getMessage());
                showPaymentLoadingFailed();
            }
        };

        new ComputationLoader().loadComputation(callback, reservation.computationId);
    }

    private void showCards() {
        // Show flight departure card
        departureFlightCard.setFlightDetails(departureFlight, 0);
        departureFlightCard.hidePrice();
        departureFlightCard.setVisibility(View.VISIBLE);

        // Show returnFlight card if provided
        if (returnFlight != null) {
            returnFlightCard.setReturnLabel();
            returnFlightCard.setFlightDetails(returnFlight, 0);
            returnFlightCard.hidePrice();
            returnFlightCard.setVisibility(View.VISIBLE);
        }

        // Show hotel card if provided
        if (hotelReservation != null) {
            hotelCard.setHotelDetails(hotel, 0);
            hotelCard.hidePrice();
            hotelCard.setVisibility(View.VISIBLE);
        }

        // Show Reservation card
        bookingIdTextView.setText(reservation.bookingId);
        bookingDateTextView.setText(TimeUtils.toFlightDisplayFormat(TimeUtils.fromIso8601(reservation.createDate)));
        statusTextView.setText(reservation.status);
        totalAmountTextView.setText("PHP " + TextUtils.toCurrencyFormat(computation.computedGrandTotal));
        reservationCardView.setVisibility(View.VISIBLE);

        // Show passengers
        adapter.setPassengers(passengers);
        passengersCardView.setVisibility(View.VISIBLE);

        progress.setVisibility(View.INVISIBLE);
        scrollView.post(
            new Runnable() {
                @Override
                public void run() {
                    scrollView.fullScroll(View.FOCUS_UP);
                }
            });
    }

    private void showFlightLoadingFailed() {
        progress.setVisibility(View.INVISIBLE);
        Toast.makeText(UserReservationActivity.this, "Loading flight failed", Toast.LENGTH_LONG)
             .show();
    }

    private void showFlightReservationLoadingFailed() {
        progress.setVisibility(View.INVISIBLE);
        Toast.makeText(UserReservationActivity.this, "Loading flight reservation failed", Toast.LENGTH_LONG)
             .show();
    }

    private void showHotelReservationLoadingFailed() {
        progress.setVisibility(View.INVISIBLE);
        Toast.makeText(UserReservationActivity.this, "Loading hotel reservation failed", Toast.LENGTH_LONG)
             .show();
    }

    private void showPaymentLoadingFailed() {
        progress.setVisibility(View.INVISIBLE);
        Toast.makeText(UserReservationActivity.this, "Loading payment reservation failed", Toast.LENGTH_LONG)
             .show();
    }

    private void setUpRecyclerView() {
        adapter = new PassengerListAdapter(passengers);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(getString(R.string.reservation_summary));
    }
}
