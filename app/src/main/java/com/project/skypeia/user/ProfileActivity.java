package com.project.skypeia.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.project.skypeia.R;
import com.project.skypeia.api.reservation.LoadReservationsResultDto;
import com.project.skypeia.home.HomeActivity;
import com.project.skypeia.reservation.Reservation;
import com.project.skypeia.reservation.ReservationListAdapter;
import com.project.skypeia.reservation.ReservationLoader;
import com.project.skypeia.util.LoginUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ProfileActivity extends AppCompatActivity {

    private List<Reservation> reservations = new ArrayList<>();

    private ReservationListAdapter adapter;

    private LoginUtils loginUtils;

    @BindView(R.id.text_view_no_reservations)
    TextView noReservationsTextView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @OnClick(R.id.book_flight_button)
    public void onClickBookFlightButton() {
        clearActivityStackAndNavigateToHomeActivity();
    }

    @OnClick(R.id.logout_button)
    public void onClickLogoutButton() {
        loginUtils.logout();
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        setUpToolbar();
        setUpRecyclerView();

        loadReservations();
        loginUtils = new LoginUtils(this);
    }

    private void clearActivityStackAndNavigateToHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void loadReservations() {
        // Callback for loading reservations
        Callback<LoadReservationsResultDto> callback = new Callback<LoadReservationsResultDto>() {
            @Override
            public void onResponse(Call<LoadReservationsResultDto> call,
                                   Response<LoadReservationsResultDto> response) {
                if (response.isSuccessful()) {
                    Reservation[] results = response.body().results;
                    if (results != null && results.length > 0) {
                        reservations = Arrays.asList(results);
                        adapter.setFlights(reservations);
                        recyclerView.setVisibility(View.VISIBLE);
                    } else {
                        noReservationsTextView.setVisibility(View.VISIBLE);
                    }
                } else {
                    showReservationLoadingFailed();
                }
            }

            @Override
            public void onFailure(Call<LoadReservationsResultDto> call, Throwable t) {
                Timber.e(t.getMessage());
                showReservationLoadingFailed();
            }
        };

        new ReservationLoader().loadReservations(callback);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(getString(R.string.my_reservations));
    }

    private void setUpRecyclerView() {
        adapter = new ReservationListAdapter(this.getBaseContext(), reservations);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void showReservationLoadingFailed() {
        Toast.makeText(ProfileActivity.this, "Loading reservations failed", Toast.LENGTH_LONG)
             .show();
    }
}
