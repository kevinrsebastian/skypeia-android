package com.project.skypeia.user;

import com.project.skypeia.Constants;
import com.project.skypeia.api.user.UserRetrofitService;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserLoader {

    private UserRetrofitService userRetrofitService;

    public UserLoader() {
        // Instantiate Retrofit with JSON converter
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        userRetrofitService = retrofit.create(UserRetrofitService.class);
    }

    public void getUserById(Callback<User> callback, String userId) {
        userRetrofitService.getUserById(userId)
                           .enqueue(callback);
    }
}
