package com.project.skypeia.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.project.skypeia.R;
import com.project.skypeia.util.LoginUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class UserLoginActivity extends AppCompatActivity {

    private LoginUtils loginUtils;

    @BindView(R.id.edit_text_user_id)
    EditText userIdEditText;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @OnClick(R.id.login)
    public void onClickLogin() {
        login();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        ButterKnife.bind(this);
        setUpToolbar();

        loginUtils = new LoginUtils(getApplicationContext());
    }

    private void login() {
        // Callback for login
        Callback<User> callback = new Callback<User>() {
            @Override
            public void onResponse(Call<User> call,
                                   Response<User> response) {
                if (response.isSuccessful()) {
                    loginUtils.login(response.body().userId);
                    setResult(RESULT_OK, new Intent());
                    finish();
                } else {
                    Toast.makeText(UserLoginActivity.this, "Login Failed", Toast.LENGTH_LONG)
                         .show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Timber.e(t.getMessage());
                Toast.makeText(UserLoginActivity.this, "Login Failed", Toast.LENGTH_LONG)
                     .show();
            }
        };

        new UserLoader().getUserById(callback, userIdEditText.getText().toString());
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("User Login");
    }
}
