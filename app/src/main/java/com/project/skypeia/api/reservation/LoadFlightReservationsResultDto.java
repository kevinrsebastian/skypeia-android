package com.project.skypeia.api.reservation;

import com.project.skypeia.reservation.FlightReservation;

public class LoadFlightReservationsResultDto {

    public FlightReservation[] results;
}
