package com.project.skypeia.api.reservation;

public class SaveReservationRequestDto {

    public String flightReservationId;

    public String hotelReservationId;

    public String couponId;

    public String computationId;

    public String userId;
}
