package com.project.skypeia.api.computation;

import com.project.skypeia.computation.Computation;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SavePaymentRetrofitService {

    @POST("payment")
    Call<Computation> saveComputation(
        @Query("flight-reservation-id") String flightReservationId,
        @Query("hotel-reservation-id") String hotelReservationId,
        @Query("coupon-and-discount-id") String couponId);
}
