package com.project.skypeia.api.airport;

import com.project.skypeia.airport.Airport;

public class FilterAirportsResponseDto {

    public int size;

    public int totalCount;

    public Airport[] results;
}
