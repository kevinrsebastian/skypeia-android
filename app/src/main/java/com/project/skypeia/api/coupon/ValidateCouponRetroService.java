package com.project.skypeia.api.coupon;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ValidateCouponRetroService {

    @POST("coupons/validate")
    Call<Boolean> validateCoupon(@Query("coupon-id") String couponId);
}
