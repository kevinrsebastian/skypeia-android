package com.project.skypeia.api.flight;

public class FilterFlightsRequestDto {

    public String origin;

    public String destination;

    public String startDepartureTime;

    public String endDepartureTime;

    public int availableSeats;
}
