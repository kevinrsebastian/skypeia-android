package com.project.skypeia.api.computation;

import com.project.skypeia.computation.Computation;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface LoadPaymentRetrofitService {

    @GET("payment/{payment-id}")
    Call<Computation> getPaymentById(
        @Path("payment-id") String paymentId);
}
