package com.project.skypeia.api.hotel;

import com.project.skypeia.api.FilterRequestDto;
import com.project.skypeia.hotel.Hotel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HotelsRetrofitService {

    @POST("hotels/filter")
    Call<FilterHotelsResponseDto> getPorts(@Body FilterRequestDto filterRequestDto);

    @GET("hotels/{hotel-id}")
    Call<Hotel> getHotelById(@Path("hotel-id") String hotelId);
}
