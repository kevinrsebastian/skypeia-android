package com.project.skypeia.api.hotel;

import com.project.skypeia.hotel.Hotel;

public class FilterHotelsResponseDto {

    public int size;

    public int totalCount;

    public Hotel[] results;
}
