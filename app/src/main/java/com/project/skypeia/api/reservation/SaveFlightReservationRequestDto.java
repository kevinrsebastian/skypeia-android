package com.project.skypeia.api.reservation;

import com.project.skypeia.passenger.Passenger;

public class SaveFlightReservationRequestDto {

    public String oneWayFlightId;

    public String roundTripFlightId;

    public Passenger[] flightPassengerDetails;

    public String userId;

    public String flightType = "ONEWAY";
}
