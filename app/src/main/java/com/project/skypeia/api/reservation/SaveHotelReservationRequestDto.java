package com.project.skypeia.api.reservation;

public class SaveHotelReservationRequestDto {

    public String hotelId;

    public int totalNumOfPax;

    public String userId;

}
