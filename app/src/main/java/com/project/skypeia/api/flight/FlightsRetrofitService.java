package com.project.skypeia.api.flight;

import com.project.skypeia.flight.Flight;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FlightsRetrofitService {

    @POST("flights/retrieve-by-details")
    Call<FilterFlightsResponseDto> getFlights(
        @Body FilterFlightsRequestDto filterFlightsRequestDto);

    @GET("flights/{flight-id}")
    Call<Flight> getFlightById(
        @Path("flight-id") String flightId);

}
