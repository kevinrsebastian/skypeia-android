package com.project.skypeia.api.reservation;

import com.project.skypeia.reservation.Reservation;

public class LoadReservationsResultDto {

    public Reservation[] results;
}
