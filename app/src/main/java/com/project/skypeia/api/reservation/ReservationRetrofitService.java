package com.project.skypeia.api.reservation;

import com.project.skypeia.api.FilterRequestDto;
import com.project.skypeia.reservation.FlightReservation;
import com.project.skypeia.reservation.HotelReservation;
import com.project.skypeia.reservation.Reservation;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ReservationRetrofitService {

    @GET("flight-reservations/{flight-reservation-id}")
    Call<FlightReservation> getFlightReservationById(
        @Path("flight-reservation-id") String flightReservationId);

    @GET("hotel-reservations/{hotel-reservation-id}")
    Call<HotelReservation> getHotelReservationById(
        @Path("hotel-reservation-id") String hotelReservationId);

    @POST("reservations/filter")
    Call<LoadReservationsResultDto> getReservations(
        @Body FilterRequestDto filterRequestDto);

    @POST("flight-reservations")
    Call<FlightReservation> saveFlightReservation(
        @Body SaveFlightReservationRequestDto saveFlightReservationRequestDto);

    @POST("hotel-reservations")
    Call<HotelReservation> saveHotelReservation(
        @Body SaveHotelReservationRequestDto saveHotelReservationRequestDto);

    @POST("reservations")
    Call<Reservation> saveReservation(
        @Body SaveReservationRequestDto saveReservationRequestDto);
}
