package com.project.skypeia.api.airport;

import com.project.skypeia.api.FilterRequestDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AirportRetrofitService {

    @POST("flight-ports/filter")
    Call<FilterAirportsResponseDto> getPorts(@Body FilterRequestDto filterRequestDto);
}
