package com.project.skypeia.api;

public class FilterRequestDto {
    int page;
    int limit;

    public FilterRequestDto(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }
}
