package com.project.skypeia.hotel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.project.skypeia.Constants;
import com.project.skypeia.R;
import com.project.skypeia.api.hotel.FilterHotelsResponseDto;
import com.project.skypeia.home.FlightSearchCriteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class HotelSelectionActivity extends AppCompatActivity {

    private List<Hotel> hotels = new ArrayList<>();

    private HotelListAdapter adapter;

    private FlightSearchCriteria flightSearchCriteria;

    @BindView(R.id.select_button)
    Button selectButton;

    @BindView(R.id.no_hotels)
    TextView noHotels;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @OnClick(R.id.select_button)
    public void onClickSelectButton() {
        Hotel hotel = adapter.getSelectedHotel();

        Intent intent = new Intent();
        intent.putExtra(Constants.IntentExtraKeys.HOTEL, hotel);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_selection);
        ButterKnife.bind(this);

        getFlightCriteriaFromIntent();
        setUpToolbar();
        setUpRecyclerView();
        setUpHotels();
    }

    private void getFlightCriteriaFromIntent() {
        flightSearchCriteria =
            getIntent().getParcelableExtra(Constants.IntentExtraKeys.FLIGHT_SELECTION_CRITERIA);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Select Hotel");
    }

    private void setUpHotels() {
        // Callback for loading hotels
        Callback<FilterHotelsResponseDto> callback = new Callback<FilterHotelsResponseDto>() {
            @Override
            public void onResponse(Call<FilterHotelsResponseDto> call,
                                   Response<FilterHotelsResponseDto> response) {
                if (response.isSuccessful()) {
                    Hotel[] hotelArray = response.body().results;
                    if (hotelArray != null) {

                        // Filter by destination location
                        List<Hotel> allHotels = Arrays.asList(hotelArray);
                        for (Hotel hotel : allHotels) {
                            if (hotel.location.id.equals(flightSearchCriteria.destinationAirport.id)) {
                                hotels.add(hotel);
                            }
                        }

                        adapter.setHotels(hotels);
                        selectButton.setEnabled(true);
                        recyclerView.setVisibility(View.VISIBLE);
                    } else {
                        selectButton.setEnabled(false);
                        noHotels.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(HotelSelectionActivity.this,
                                   "Loading hotels failed",
                                   Toast.LENGTH_LONG)
                         .show();
                }
            }

            @Override
            public void onFailure(Call<FilterHotelsResponseDto> call, Throwable t) {
                Timber.e(t.getMessage());
                Toast.makeText(HotelSelectionActivity.this,
                               "Loading hotels failed",
                               Toast.LENGTH_LONG)
                     .show();
            }
        };

        new HotelLoader().getHotels(callback);
    }

    private void setUpRecyclerView() {
        adapter = new HotelListAdapter(hotels);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
