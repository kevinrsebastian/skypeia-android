package com.project.skypeia.hotel;

import com.project.skypeia.Constants;
import com.project.skypeia.api.FilterRequestDto;
import com.project.skypeia.api.hotel.FilterHotelsResponseDto;
import com.project.skypeia.api.hotel.HotelsRetrofitService;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HotelLoader {

    private HotelsRetrofitService hotelsRetrofitService;

    public HotelLoader() {
        // Instantiate Retrofit with JSON converter
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(Constants.BASE_URL);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        hotelsRetrofitService = retrofit.create(HotelsRetrofitService.class);
    }

    public void getHotels(Callback<FilterHotelsResponseDto> callback) {
        hotelsRetrofitService.getPorts(new FilterRequestDto(0, 0))
                             .enqueue(callback);
    }

    public void getHotel(Callback<Hotel> callback, String hotelId) {
        hotelsRetrofitService.getHotelById(hotelId)
                             .enqueue(callback);
    }
}
