package com.project.skypeia.hotel;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.skypeia.R;
import com.project.skypeia.util.TextUtils;

import java.util.ArrayList;
import java.util.List;

public class HotelListAdapter extends RecyclerView.Adapter<HotelViewHolder> {

    private List<Hotel> hotels = new ArrayList<>();

    private int lastCheckedPosition = -1;

    public HotelListAdapter(List<Hotel> hotels) {
        this.hotels = hotels;
    }

    @Override
    public HotelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_hotel, parent, false);
        return new HotelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HotelViewHolder holder, final int position) {
        Hotel hotel = hotels.get(position);
        holder.name.setText(hotel.hotelName);
        holder.price.setText("PHP " + TextUtils.toCurrencyFormat(hotel.hotelCostPrePax));
        holder.address.setText(hotel.address);
        holder.contact.setText(hotel.contactPerson + " (" + hotel.contactNumber + ")");

        // Set the ui attributes of the selected item
        if (position == lastCheckedPosition) {
            holder.selection.setChecked(true);
        } else {
            holder.selection.setChecked(false);
        }

        // Set a listener that sets the currently clicked item as the last checked position
        // then restart the items
        holder.selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastCheckedPosition = position;
                notifyDataSetChanged();
            }
        });

        // Set the default selected
        if (position == 0 && lastCheckedPosition == -1) {
            holder.selection.setChecked(true);
            lastCheckedPosition = 0;
        }
    }

    @Override
    public int getItemCount() {
        return hotels.size();
    }

    public Hotel getSelectedHotel() {
        return hotels.get(lastCheckedPosition);
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
        notifyDataSetChanged();
    }
}
