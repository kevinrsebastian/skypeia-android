package com.project.skypeia.hotel;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.project.skypeia.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HotelViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.radio_button_selection)
    RadioButton selection;

    @BindView(R.id.text_view_name)
    TextView name;

    @BindView(R.id.text_view_price)
    TextView price;

    @BindView(R.id.text_view_address)
    TextView address;

    @BindView(R.id.text_view_contact)
    TextView contact;

    public HotelViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
