package com.project.skypeia.hotel;

import android.os.Parcel;
import android.os.Parcelable;

import com.project.skypeia.airport.Airport;

public class Hotel implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Hotel createFromParcel(Parcel in) {
            return new Hotel(in);
        }

        public Hotel[] newArray(int size) {
            return new Hotel[size];
        }
    };

    public String hotelId;

    public String hotelName;

    public long availableSlots;

    public Airport location;

    public String address;

    public String contactPerson;

    public String contactNumber;

    public float hotelCostPrePax;

    // Default Constructor
    public Hotel() {
        this.hotelId = "";
        this.hotelName = "";
        this.availableSlots = 0;
        this.address = "";
        this.contactPerson = "";
        this.contactNumber = "";
        this.hotelCostPrePax = 0;
    }

    // Parcelable Constructor
    public Hotel(Parcel in) {
        this.hotelId = in.readString();
        this.hotelName = in.readString();
        this.availableSlots = in.readLong();
        this.address = in.readString();
        this.contactPerson = in.readString();
        this.contactNumber = in.readString();
        this.hotelCostPrePax = in.readFloat();
        this.location = in.readParcelable(Airport.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Must be in order to be retrieved correctly
        dest.writeString(this.hotelId);
        dest.writeString(this.hotelName);
        dest.writeLong(this.availableSlots);
        dest.writeString(this.address);
        dest.writeString(this.contactPerson);
        dest.writeString(this.contactNumber);
        dest.writeFloat(this.hotelCostPrePax);
        dest.writeParcelable(this.location, flags);
    }
}
