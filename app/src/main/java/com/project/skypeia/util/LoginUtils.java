package com.project.skypeia.util;

import android.content.Context;
import android.content.SharedPreferences;


import static android.content.Context.MODE_PRIVATE;

public class LoginUtils {

    private SharedPreferences sharedPreferences;

    public LoginUtils(Context context) {
        String sharedPreferencesName = context.getPackageName() + "_preferences";
        sharedPreferences = context.getSharedPreferences(sharedPreferencesName, MODE_PRIVATE);
    }

    public String getLoggedUserId() {
        return sharedPreferences.getString("USER_ID", null);
    }

    public void login(String userId) {
        sharedPreferences.edit().putString("USER_ID", userId).apply();
    }

    public void logout() {
        sharedPreferences.edit().remove("USER_ID").apply();
    }
}
