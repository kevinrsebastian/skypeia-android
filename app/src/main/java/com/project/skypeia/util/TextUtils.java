package com.project.skypeia.util;

import java.text.DecimalFormat;

public class TextUtils {

    public static String toCurrencyFormat(float amount) {
        return new DecimalFormat("#.00").format(amount);
    }
}
