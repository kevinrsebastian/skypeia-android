package com.project.skypeia.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TimeUtils {

    public static final String HOUR_MINUTE_FORMAT = "HHmm";
    public static final String HOUR_MINUTE_SECOND_FORMAT = "HH:mm:ss";
    public static final String ISO_8601_FORMAT =
        "yyyy-MM-dd'T'" + HOUR_MINUTE_SECOND_FORMAT;
    public static final String ISO_8601_FORMAT_WITH_MILLISECONDS =
        "yyyy-MM-dd'T'" + HOUR_MINUTE_SECOND_FORMAT;
    public static final String FLIGHT_DISPLAY_FORMAT = "h:mm a dd MMM yyyy";
    public static final String DOB_DISPLAY_FORMAT = "dd MMM yyyy";

    public static long removeTimeUnits(long dateMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateMillis);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static long addDays(long dateMillis, int numberOfDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateMillis);
        calendar.add(Calendar.DATE, numberOfDays);
        return calendar.getTimeInMillis();
    }

    public static long fromIso8601(String dateTime) {
        try {
            return new SimpleDateFormat(ISO_8601_FORMAT_WITH_MILLISECONDS, Locale.UK)
                .parse(dateTime)
                .getTime();
        } catch (Exception e) {
            return 0;
        }

    }

    public static String toHourMinute(long date) {
        return new SimpleDateFormat(HOUR_MINUTE_FORMAT, Locale.UK).format(date) + " H";
    }

    public static String toBirthDateDisplayFormat(long date) {
        return new SimpleDateFormat(DOB_DISPLAY_FORMAT, Locale.UK).format(date);
    }

    public static String toFlightDisplayFormat(long date) {
        return new SimpleDateFormat(FLIGHT_DISPLAY_FORMAT, Locale.UK).format(date);
    }

    public static String toIso8601(long date) {
        String iso8901 =  new SimpleDateFormat(ISO_8601_FORMAT,
                                               Locale.UK).format(date);
        return iso8901 + ".000Z";
    }
}
