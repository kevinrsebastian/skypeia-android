package com.project.skypeia.widget;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.project.skypeia.R;
import com.project.skypeia.hotel.Hotel;
import com.project.skypeia.util.TextUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HotelCard extends CardView {

    @BindView(R.id.text_view_hotel_name)
    TextView name;

    @BindView(R.id.text_view_hotel_address)
    TextView address;

    @BindView(R.id.text_view_hotel_contact)
    TextView contact;

    @BindView(R.id.text_view_hotel_price)
    TextView price;

    @BindView(R.id.text_view_hotel_price_label)
    TextView priceLabel;

    @BindView(R.id.bottom_divider)
    View bottomDivider;

    public HotelCard(Context context) {
        super(context);
        initialise();
    }

    public HotelCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialise();
    }

    public HotelCard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialise();
    }

    public void hidePrice() {
        bottomDivider.setVisibility(GONE);
        price.setVisibility(GONE);
        priceLabel.setVisibility(GONE);
    }

    public void setHotelDetails(Hotel hotel, float totalPrice) {
        name.setText(hotel.hotelName);
        address.setText(hotel.address);
        contact.setText(hotel.contactPerson + " (" + hotel.contactNumber + ")");
        price.setText("PHP " + TextUtils.toCurrencyFormat(totalPrice));
    }

    private void initialise() {
        inflate(getContext(), R.layout.layout_hotel_card, this);
        ButterKnife.bind(this);
    }
}
