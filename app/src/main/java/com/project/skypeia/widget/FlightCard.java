package com.project.skypeia.widget;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.project.skypeia.R;
import com.project.skypeia.flight.Flight;
import com.project.skypeia.util.TextUtils;
import com.project.skypeia.util.TimeUtils;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FlightCard extends CardView {

    @BindString(R.string.departure_flight)
    String departureFlightString;

    @BindString(R.string.return_flight)
    String returnFlightString;

    @BindView(R.id.text_view_label)
    TextView headerLabel;

    @BindView(R.id.text_view_airline_flight)
    TextView airlineAndNo;

    @BindView(R.id.text_view_airports)
    TextView airports;

    @BindView(R.id.text_view_departure_time)
    TextView departureTime;

    @BindView(R.id.text_view_arrival_time)
    TextView arrivalTime;

    @BindView(R.id.text_view_duration)
    TextView estDuration;

    @BindView(R.id.text_view_aircraft)
    TextView aircraft;

    @BindView(R.id.text_view_price)
    TextView price;

    @BindView(R.id.text_view_price_label)
    TextView priceLabel;

    @BindView(R.id.bottom_divider)
    View bottomDivider;

    public FlightCard(Context context) {
        super(context);
        initialise();
    }

    public FlightCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialise();
    }

    public FlightCard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialise();
    }

    public void hidePrice() {
        bottomDivider.setVisibility(GONE);
        price.setVisibility(GONE);
        priceLabel.setVisibility(GONE);
    }

    public void setReturnLabel() {
        headerLabel.setText(returnFlightString);
    }

    public void setFlightDetails(Flight flight, float totalPrice) {
        airlineAndNo.setText(flight.airline + " " + flight.number);
        airports.setText(flight.originAirport.toLabel() + " to " + flight.destinationAirport.toLabel());

        // Set departure / arrival time
        departureTime.setText(TimeUtils.toFlightDisplayFormat(TimeUtils.fromIso8601(flight.departureTime)));
        arrivalTime.setText(TimeUtils.toFlightDisplayFormat(TimeUtils.fromIso8601(flight.arrivalTime)));

        estDuration.setText(flight.duration);
        aircraft.setText(flight.aircraftType);

        price.setText("PHP " + TextUtils.toCurrencyFormat(totalPrice));
    }

    private void initialise() {
        inflate(getContext(), R.layout.layout_flight_card, this);
        ButterKnife.bind(this);
    }
}
